Antifraud Stream Simulator
=========================

Getting started
------------------------------
Make sure [Python 3.6](https://www.python.org/downloads/release/python-360/) or greater is installed.

Create a virtual environment and install dependencies:

    cd softconstruct-testtask

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt

Run the webpage:

    python webpage.py
    open a browser on 0.0.0.0:8080

In a different terminal, run the server:

    cd softconstruct-testtask

    source venv/bin/activate
    python server.py

And in a different terminal, run the client:

    cd softconstruct-testtask

    source venv/bin/activate
    python client.py
