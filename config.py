import os


HOST = os.getenv('HOST', '0.0.0.0')
PORT = int(os.getenv('PORT', 8765))

WEB_PORT = int(os.getenv('PORT', 8080))
N_SAMPLE = int(os.getenv('SAMPLE', 10_000_000))
OVER_STD_DEV_RANGE = int(os.getenv('RANGE', 2))
