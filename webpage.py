import os
import aiohttp.web

from config import HOST, WEB_PORT


HTML = os.path.join(os.path.dirname(__file__), 'index.html')


async def ws_handler(request):
    resp = aiohttp.web.WebSocketResponse()
    available = resp.can_prepare(request)

    if not available:
        with open(HTML, 'rb') as f:
            return aiohttp.web.Response(body=f.read(), content_type='text/html')

    await resp.prepare(request)

    try:
        request.app['sockets'].append(resp)

        async for msg in resp:
            if msg.type == aiohttp.web.WSMsgType.TEXT:
                for ws in request.app['sockets']:
                    if ws is not resp:
                        await ws.send_str(msg.data)
            else:
                return resp
        return resp

    finally:
        request.app['sockets'].remove(resp)


async def on_shutdown(app):
    for ws in app['sockets']:
        await ws.close()


if __name__ == '__main__':
    app = aiohttp.web.Application()
    app['sockets'] = []

    app.router.add_get('/', ws_handler)
    app.on_shutdown.append(on_shutdown)

    aiohttp.web.run_app(app, host=HOST, port=WEB_PORT)
