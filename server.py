import numpy as np
import logging
import aiohttp.web
import asyncio

from config import HOST, PORT, N_SAMPLE
from random import uniform


logging.basicConfig(level=logging.INFO)


def create_distribution(length=N_SAMPLE):
    for num in np.random.normal(0, 1, length):
        yield num


async def ws_handler(request):

    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    client = await ws.receive_str()
    logging.info(f'{client} has been connected')
    for i, num in enumerate(create_distribution()):
        data = (i, num)
        await ws.send_str(str(data))
        await asyncio.sleep(uniform(0.1, 0.5))
    else:
        await ws.close()

    logging.info(f'{client} connection is closed')
    return ws


if __name__ == '__main__':
    app = aiohttp.web.Application()
    app.router.add_get('/ws', ws_handler)
    aiohttp.web.run_app(app, host=HOST, port=PORT)
