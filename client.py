import aiohttp.web
import asyncio

from config import HOST, PORT, WEB_PORT, OVER_STD_DEV_RANGE
from datetime import datetime


async def log(message):
    sequence, number = eval(message)

    if -OVER_STD_DEV_RANGE > number or number > OVER_STD_DEV_RANGE:

        try:
            async with aiohttp.ClientSession().ws_connect(f'ws://{HOST}:{WEB_PORT}', autoclose=True) as ws:
                data = {
                    'timestamp': datetime.now().strftime("%m/%d/%Y-%H:%M:%S:%f"),
                    'sequence': sequence,
                    'number': number
                }

                await ws.send_str('> {timestamp}: seq={sequence} num={number}'.format(**data))
        except Exception as e:
            print(f'Connection is lost: {e.args}')
    else:
        print(message)


async def client():
    try:
        async with aiohttp.ClientSession().ws_connect(f'http://{HOST}:{PORT}/ws', autoclose=True) as ws:
            await ws.send_str(f'{HOST}:{PORT}')

            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    message = msg.data
                    await log(message=message)
                elif msg.type in (aiohttp.WSMsgType.CLOSED, aiohttp.WSMsgType.ERROR):
                    break
    except Exception as e:
        print(f'Connection is lost: {e.args}')


if __name__ == '__main__':
    asyncio.run(client())
